package com.mm.ryanairtechnicaltask.view.fragments

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.mm.ryanairtechnicaltask.R
import com.mm.ryanairtechnicaltask.model.AvailableFlightsRequest
import com.mm.ryanairtechnicaltask.model.Station
import com.mm.ryanairtechnicaltask.utils.DataState
import com.mm.ryanairtechnicaltask.viewmodel.StationsStateEvent
import com.mm.ryanairtechnicaltask.viewmodel.StationsViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_search_flights.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.util.*

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class SearchFlightsFragment : Fragment() {

    private val viewModel: StationsViewModel by viewModels()
    private lateinit var container: ConstraintLayout

    private lateinit var stations: List<Station>
    private var originStations = mutableListOf<String>()
    private var destinationStations = mutableListOf<String>()

    private var selectedOrigin: Station = Station("", "")
    private var selectedDestination: Station = Station("", "")

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search_flights, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        container = view as ConstraintLayout

        subscribeObservers()
        viewModel.setStateEvent(StationsStateEvent.GetStationEvents)

        origin_station_field.setOnClickListener {
            prepareOriginPicker()
        }
        destination_station_field.setOnClickListener {
            prepareDestinationPicker()
        }
        prepareDatePicker()

        search_button.setOnClickListener {
            gatherFormDataAndSearch()
        }
    }

    private fun subscribeObservers() {
        viewModel.dataState.observe(requireActivity(), Observer { dataState ->
            when (dataState) {
                is DataState.Success<List<Station>> -> {
                    showProgressBar(false)
                    stations = dataState.data
                    fetchStationsNames()
                }
                is DataState.Loading -> {
                    showProgressBar(true)
                }
                is DataState.Error -> {
                    showProgressBar(false)
                    Toast.makeText(
                        requireContext(),
                        dataState.exception.message.toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

        })
    }

    private fun showProgressBar(isDisplayed: Boolean) {
        progress_bar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }

    private fun fetchStationsNames() {
        for (station in stations) {
            originStations.add(station.name)
            destinationStations.add(station.name)
        }
    }

    private fun prepareOriginPicker() {
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle(resources.getString(R.string.origin_station_hint))

        builder.setSingleChoiceItems(originStations.toTypedArray(), 0) { dialog, which ->
            origin_station_field.setText(originStations[which])
            selectedOrigin = stations[which]
        }

        builder.setPositiveButton("OK") { dialog, which ->
            dialog.cancel()
        }
        builder.setNegativeButton("Cancel", null)

        val dialog = builder.create()
        dialog.show()
    }

    private fun prepareDestinationPicker() {
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle(resources.getString(R.string.destination_station_hint))

        builder.setSingleChoiceItems(destinationStations.toTypedArray(), 0) { dialog, which ->
            destination_station_field.setText(destinationStations[which])
            selectedDestination = stations[which]
        }

        builder.setPositiveButton("OK") { dialog, which ->
            dialog.cancel()
        }
        builder.setNegativeButton("Cancel", null)

        val dialog = builder.create()
        dialog.show()
    }

    private fun prepareDatePicker() {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(
            requireContext(),
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                departure_date_field.setText(
                    String.format(
                        resources.getString(R.string.departure_date),
                        year,
                        monthOfYear + 1,
                        dayOfMonth
                    )
                )
            },
            year,
            month,
            day
        )

        departure_date_field.setOnClickListener {
            dpd.show()
        }
    }

    private fun gatherFormDataAndSearch() {
        val adults = adults_field.text.toString()
        val teens = teens_field.text.toString()
        val children = children_field.text.toString()
        if (selectedOrigin.code.isNotEmpty() && selectedDestination.code.isNotEmpty() && departure_date_field.text.toString().isNotEmpty()) {
            val searchData = AvailableFlightsRequest(
                selectedOrigin.code,
                selectedOrigin.name,
                selectedDestination.code,
                selectedDestination.name,
                departure_date_field.text.toString(),
                if (adults.isNotEmpty()) Integer.parseInt(adults) else 0,
                if (adults.isNotEmpty()) Integer.parseInt(teens) else 0,
                if (adults.isNotEmpty()) Integer.parseInt(children) else 0
            )
            val bundle = bundleOf("data" to searchData)
            Navigation.findNavController(container)
                .navigate(R.id.action_searchFlightsFragment_to_flightsFragment, bundle)
        } else {
            Toast.makeText(requireContext(), resources.getString(R.string.form_not_filled), Toast.LENGTH_SHORT).show()
        }
    }
}