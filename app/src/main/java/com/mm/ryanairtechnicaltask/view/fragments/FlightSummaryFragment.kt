package com.mm.ryanairtechnicaltask.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.mm.ryanairtechnicaltask.R
import com.mm.ryanairtechnicaltask.model.FlightOnListRepresentation
import kotlinx.android.synthetic.main.fragment_flight_summary.*

class FlightSummaryFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_flight_summary, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val flight = arguments?.get("flight") as FlightOnListRepresentation
        val origin = arguments?.getString("origin")
        val destination = arguments?.getString("destination")

        val toolbar = (activity as AppCompatActivity).supportActionBar
        toolbar?.title = "$origin -> $destination"
        toolbar?.setDisplayHomeAsUpEnabled(true)

        destination_value.text = destination
        origin_value.text = origin
        infants_value.text = flight.flight.infantsLeft.toString()
        fare_class_value.text = flight.flight.regularFare.fareClass
        discount_value.text = flight.flight.regularFare.fares[0].discountInPercent.toString()
    }
}