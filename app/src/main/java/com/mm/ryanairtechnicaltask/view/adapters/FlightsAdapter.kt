package com.mm.ryanairtechnicaltask.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mm.ryanairtechnicaltask.R
import com.mm.ryanairtechnicaltask.model.FlightOnListRepresentation
import kotlinx.android.synthetic.main.item_flight.view.*

class FlightsAdapter(
    private val flights: List<FlightOnListRepresentation>,
    private val clickListener: (FlightOnListRepresentation) -> Unit
) : RecyclerView.Adapter<FlightsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_flight, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = flights.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val flight = flights[position]

        holder.view.flight_date.text = flight.flight.time[0]
        holder.view.flight_number.text = flight.flight.flightNumber
        holder.view.duration.text = flight.flight.duration
        holder.view.price.text =
            String.format("%d%s", flight.flight.regularFare.fares[0].amount, flight.currency)

        holder.itemView.setOnClickListener {
            clickListener.invoke(flight)
        }
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}