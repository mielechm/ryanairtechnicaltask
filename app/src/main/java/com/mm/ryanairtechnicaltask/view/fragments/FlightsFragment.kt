package com.mm.ryanairtechnicaltask.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.mm.ryanairtechnicaltask.R
import com.mm.ryanairtechnicaltask.model.AvailableFlightsRequest
import com.mm.ryanairtechnicaltask.model.FlightOnListRepresentation
import com.mm.ryanairtechnicaltask.model.network.responses.FlightSearchResponse
import com.mm.ryanairtechnicaltask.utils.DataState
import com.mm.ryanairtechnicaltask.view.adapters.FlightsAdapter
import com.mm.ryanairtechnicaltask.viewmodel.FlightSearchResponseStateEvent
import com.mm.ryanairtechnicaltask.viewmodel.FlightsViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_flights.*
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class FlightsFragment : Fragment() {

    private lateinit var container: ConstraintLayout
    private val viewModel: FlightsViewModel by viewModels()
    private val flights = mutableListOf<FlightOnListRepresentation>()
    private lateinit var adapter: FlightsAdapter

    private lateinit var searchData: AvailableFlightsRequest

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_flights, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        container = view as ConstraintLayout
        searchData = arguments?.get("data") as AvailableFlightsRequest

        val toolbar = (activity as AppCompatActivity).supportActionBar
        toolbar?.title = "${searchData.originName} -> ${searchData.destinationName}"
        toolbar?.setDisplayHomeAsUpEnabled(true)

        subscribeObservers()
        viewModel.setStateEvent(
            searchData,
            FlightSearchResponseStateEvent.GetFlightSearchResponseEvents
        )
    }


    private fun subscribeObservers() {
        viewModel.dataState.observe(requireActivity(), Observer { dataState ->
            when (dataState) {
                is DataState.Success<FlightSearchResponse> -> {
                    showProgressBar(false)
                    no_flights_error_text.visibility = View.GONE
                    fetchFlightsData(dataState.data)
                }
                is DataState.Loading -> {
                    showProgressBar(true)
                }
                is DataState.Error -> {
                    showProgressBar(false)
                    no_flights_error_text.text = dataState.exception.message.toString()
                    no_flights_error_text.visibility = View.VISIBLE
                }
            }

        })
    }

    private fun showProgressBar(isDisplayed: Boolean) {
        progress_bar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }

    private fun fetchFlightsData(data: FlightSearchResponse) {
        for (tripDates in data.trips[0].dates) {
            for (flight in tripDates.flights) {
                flights.add(FlightOnListRepresentation(flight, data.currency))
            }
        }

        adapter = FlightsAdapter(flights) {flight ->
            val bundle = bundleOf("flight" to flight, "origin" to searchData.originName, "destination" to searchData.destinationName)
            Navigation.findNavController(container).navigate(R.id.action_flightsFragment_to_flightSummaryFragment, bundle)
        }
        flights_recycler_view.apply {
            setHasFixedSize(false)
            layoutManager = LinearLayoutManager(context)
            adapter = this@FlightsFragment.adapter
        }
        flights_recycler_view.addItemDecoration(
            DividerItemDecoration(flights_recycler_view.context, DividerItemDecoration.VERTICAL)
        )
    }
}