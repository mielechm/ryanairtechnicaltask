package com.mm.ryanairtechnicaltask.di

import com.mm.ryanairtechnicaltask.model.db.ProjectDao
import com.mm.ryanairtechnicaltask.model.db.mappers.StationCacheMapper
import com.mm.ryanairtechnicaltask.model.network.api.FlightsService
import com.mm.ryanairtechnicaltask.model.network.api.StationsService
import com.mm.ryanairtechnicaltask.model.network.mappers.StationsNetworkMapper
import com.mm.ryanairtechnicaltask.repository.FlightsRepository
import com.mm.ryanairtechnicaltask.repository.StationsRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object RepositoryModule {

    @Singleton
    @Provides
    fun provideStationsRepository(
        projectDao: ProjectDao,
        stationsService: StationsService,
        stationCacheMapper: StationCacheMapper,
        networkMapper: StationsNetworkMapper
    ): StationsRepository {
        return StationsRepository(projectDao, stationsService, stationCacheMapper, networkMapper)
    }

    @Singleton
    @Provides
    fun provideFlightsRepository(
        flightsService: FlightsService
    ): FlightsRepository {
        return FlightsRepository(flightsService)
    }
}