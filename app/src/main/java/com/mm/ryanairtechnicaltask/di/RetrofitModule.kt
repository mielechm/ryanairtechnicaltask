package com.mm.ryanairtechnicaltask.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.mm.ryanairtechnicaltask.model.network.api.FlightsService
import com.mm.ryanairtechnicaltask.model.network.api.StationsService
import com.mm.ryanairtechnicaltask.utils.BASE_RYANAIR_URL
import com.mm.ryanairtechnicaltask.utils.BASE_STATIONS_URL
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton


@Module
@InstallIn(ApplicationComponent::class)
object RetrofitModule {

    @Singleton
    @Provides
    fun provideGsonBuilder(): Gson {
        return GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()
    }

    @Singleton
    @Provides
    @Named("provideStationsRetrofit")
    fun provideStationsRetrofit(gson: Gson): Retrofit.Builder {
        return Retrofit.Builder()
            .baseUrl(BASE_STATIONS_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
    }

    @Singleton
    @Provides
    fun provideStationsService(@Named("provideStationsRetrofit") retrofit: Retrofit.Builder): StationsService {
        return retrofit.build().create(StationsService::class.java)
    }

    @Singleton
    @Provides
    @Named("provideRyanairRetrofit")
    fun provideFlightsRetrofit(gson: Gson): Retrofit.Builder {
        return Retrofit.Builder()
            .baseUrl(BASE_RYANAIR_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
    }

    @Singleton
    @Provides
    fun provideFlightsService(@Named("provideRyanairRetrofit") retrofit: Retrofit.Builder): FlightsService {
        return retrofit.build().create(FlightsService::class.java)
    }

}