package com.mm.ryanairtechnicaltask.di

import android.content.Context
import androidx.room.Room
import com.mm.ryanairtechnicaltask.model.db.ProjectDao
import com.mm.ryanairtechnicaltask.model.db.ProjectDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object DbModule {

    @Singleton
    @Provides
    fun provideStationDb(@ApplicationContext context: Context): ProjectDatabase {
        return Room.databaseBuilder(
            context,
            ProjectDatabase::class.java,
            ProjectDatabase.DATABASE_NAME
        )
            .fallbackToDestructiveMigration().build()
    }

    @Singleton
    @Provides
    fun provideStationDao(projectDatabase: ProjectDatabase): ProjectDao {
        return projectDatabase.projectDao()
    }
}