package com.mm.ryanairtechnicaltask.utils

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("SimpleDateFormat")
fun Date.toSimpleString() : String {
    val format = SimpleDateFormat("yyyy-MM-dd")
    return format.format(this)
}

@SuppressLint("SimpleDateFormat")
fun String.toDate(): Date? {
    val format = SimpleDateFormat("yyyy-MM-dd")
    return format.parse(this)
}