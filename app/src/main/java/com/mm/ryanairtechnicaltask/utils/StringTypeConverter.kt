package com.mm.ryanairtechnicaltask.utils

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

class StringTypeConverter {

    private val gson = Gson()

    @TypeConverter
    fun listToString(stringData: MutableList<String>): String = gson.toJson(stringData)

    @TypeConverter
    fun stringToList(data: String?): MutableList<String> {
        if (data == null) {
            return Collections.emptyList()
        }
        val listType = object : TypeToken<MutableList<String>>() {}.type

        return gson.fromJson(data, listType)
    }
}