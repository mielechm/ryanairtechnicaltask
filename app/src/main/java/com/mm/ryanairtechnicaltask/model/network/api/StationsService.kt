package com.mm.ryanairtechnicaltask.model.network.api

import com.mm.ryanairtechnicaltask.model.network.responses.StationsResponse
import retrofit2.http.GET

interface StationsService {

    @GET("static/stations.json")
    suspend fun getStations(): StationsResponse

}