package com.mm.ryanairtechnicaltask.model.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    tableName = "fare",
    foreignKeys = [
        ForeignKey(
            entity = RegularFareCacheEntity::class,
            parentColumns = ["fareKey"],
            childColumns = ["id"]
        )
    ]
)
data class FareCacheEntity(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long,

    @ColumnInfo(name = "amount")
    var amount: Double,

    @ColumnInfo(name = "publishedFare")
    var publishedFare: Double,

    @ColumnInfo(name = "discountInPercent")
    var discountInPercent: Double
)