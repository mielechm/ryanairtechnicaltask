package com.mm.ryanairtechnicaltask.model

data class Station(
    var code: String,
    var name: String
)