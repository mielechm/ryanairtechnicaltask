package com.mm.ryanairtechnicaltask.model.network.mappers

import com.mm.ryanairtechnicaltask.model.Flight
import com.mm.ryanairtechnicaltask.model.network.networkentities.FlightNetworkEntity
import com.mm.ryanairtechnicaltask.utils.EntityMapper
import javax.inject.Inject

class FlightNetworkMapper
@Inject
constructor() : EntityMapper<FlightNetworkEntity, Flight> {
    override fun mapFromEntity(entity: FlightNetworkEntity): Flight {
        return Flight(
            flightKey = entity.flightKey,
            duration = entity.duration,
            flightNumber = entity.flightNumber,
            infantsLeft = entity.infantsLeft,
            time = entity.time
        )
    }

    override fun mapToEntity(domainModel: Flight): FlightNetworkEntity {
        return FlightNetworkEntity(
            flightKey = domainModel.flightKey,
            duration = domainModel.duration,
            flightNumber = domainModel.flightNumber,
            infantsLeft = domainModel.infantsLeft,
            time = domainModel.time
        )
    }

    fun mapFromEntityList(entities: List<FlightNetworkEntity>): List<Flight> {
        return entities.map {
            mapFromEntity(it)
        }
    }
}