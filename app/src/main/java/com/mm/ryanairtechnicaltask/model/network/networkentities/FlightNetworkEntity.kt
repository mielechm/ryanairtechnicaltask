package com.mm.ryanairtechnicaltask.model.network.networkentities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class FlightNetworkEntity(

    @SerializedName("flightKey")
    @Expose
    var flightKey: String,

    @SerializedName("infantsLeft")
    @Expose
    var infantsLeft: Int,

    @SerializedName("flightNumber")
    @Expose
    var flightNumber: String,

    @SerializedName("duration")
    @Expose
    var duration: String,

    @SerializedName("time")
    @Expose
    var time: List<String>
)