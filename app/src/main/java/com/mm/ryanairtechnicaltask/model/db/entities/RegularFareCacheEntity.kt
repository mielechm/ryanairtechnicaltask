package com.mm.ryanairtechnicaltask.model.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "regularFare")
data class RegularFareCacheEntity(

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "fareKey")
    var fareKey: String,

    @ColumnInfo(name = "fareClass")
    var fareClass: String
) {
    @Ignore
    var fares: List<FareCacheEntity> = listOf()
}