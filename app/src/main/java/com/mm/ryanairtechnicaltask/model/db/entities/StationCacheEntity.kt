package com.mm.ryanairtechnicaltask.model.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "stations")
data class StationCacheEntity(

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "code")
    var code: String,

    @ColumnInfo(name = "name")
    var name: String
)