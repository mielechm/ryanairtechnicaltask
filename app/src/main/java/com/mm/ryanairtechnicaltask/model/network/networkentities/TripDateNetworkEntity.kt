package com.mm.ryanairtechnicaltask.model.network.networkentities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.mm.ryanairtechnicaltask.model.Flight

data class TripDateNetworkEntity(
    @SerializedName("dateOut")
    @Expose
    var dateOut: String,

    @SerializedName("flights")
    @Expose
    var flights: List<Flight>
)