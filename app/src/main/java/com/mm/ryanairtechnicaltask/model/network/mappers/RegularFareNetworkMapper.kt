package com.mm.ryanairtechnicaltask.model.network.mappers

import com.mm.ryanairtechnicaltask.model.RegularFare
import com.mm.ryanairtechnicaltask.model.network.networkentities.RegularFareNetworkEntity
import com.mm.ryanairtechnicaltask.utils.EntityMapper
import javax.inject.Inject

class RegularFareNetworkMapper
@Inject
constructor() : EntityMapper<RegularFareNetworkEntity, RegularFare> {
    override fun mapFromEntity(entity: RegularFareNetworkEntity): RegularFare {
        return RegularFare(
            fareKey = entity.fareKey,
            fareClass = entity.fareClass,
            fares = entity.fares
        )
    }

    override fun mapToEntity(domainModel: RegularFare): RegularFareNetworkEntity {
        return RegularFareNetworkEntity(
            fareKey = domainModel.fareKey,
            fareClass = domainModel.fareClass,
            fares = domainModel.fares
        )
    }

    fun mapFromEntityList(entities: List<RegularFareNetworkEntity>): List<RegularFare> {
        return entities.map {
            mapFromEntity(it)
        }
    }
}