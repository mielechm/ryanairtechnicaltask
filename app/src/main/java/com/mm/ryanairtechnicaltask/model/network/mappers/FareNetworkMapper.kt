package com.mm.ryanairtechnicaltask.model.network.mappers

import com.mm.ryanairtechnicaltask.model.Fare
import com.mm.ryanairtechnicaltask.model.network.networkentities.FareNetworkEntity
import com.mm.ryanairtechnicaltask.utils.EntityMapper
import javax.inject.Inject

class FareNetworkMapper
@Inject
constructor() : EntityMapper<FareNetworkEntity, Fare> {
    override fun mapFromEntity(entity: FareNetworkEntity): Fare {
        return Fare(
            amount = entity.amount,
            discountInPercent = entity.discountInPercent,
            publishedFare = entity.publishedFare
        )
    }

    override fun mapToEntity(domainModel: Fare): FareNetworkEntity {
        return FareNetworkEntity(
            amount = domainModel.amount,
            discountInPercent = domainModel.discountInPercent,
            publishedFare = domainModel.publishedFare
        )
    }

    fun mapFromEntityList(entities: List<FareNetworkEntity>): List<Fare> {
        return entities.map {
            mapFromEntity(it)
        }
    }
}