package com.mm.ryanairtechnicaltask.model.db.mappers

import com.mm.ryanairtechnicaltask.model.Station
import com.mm.ryanairtechnicaltask.model.db.entities.StationCacheEntity
import com.mm.ryanairtechnicaltask.utils.EntityMapper
import javax.inject.Inject

class StationCacheMapper
@Inject
constructor(): EntityMapper<StationCacheEntity, Station>{
    override fun mapFromEntity(entity: StationCacheEntity): Station {
        return Station(
            code = entity.code,
            name = entity.name
        )
    }

    override fun mapToEntity(domainModel: Station): StationCacheEntity {
        return StationCacheEntity(
            code = domainModel.code,
            name = domainModel.name
        )
    }


    fun mapFromEntityList(entities: List<StationCacheEntity>): List<Station> {
        return entities.map {
            mapFromEntity(it)
        }
    }
}