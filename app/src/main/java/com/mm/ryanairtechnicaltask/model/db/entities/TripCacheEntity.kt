package com.mm.ryanairtechnicaltask.model.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "trip")
data class TripCacheEntity(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long,

    @ColumnInfo(name = "origin")
    var origin: String,

    @ColumnInfo(name = "originName")
    var originName: String,

    @ColumnInfo(name = "destination")
    var destination: String,

    @ColumnInfo(name = "destinationName")
    var destinationName: String
) {
    @Ignore
    var dates: List<TripDateCacheEntity> = listOf()
}