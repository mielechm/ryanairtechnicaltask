package com.mm.ryanairtechnicaltask.model.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.mm.ryanairtechnicaltask.model.db.entities.*
import com.mm.ryanairtechnicaltask.utils.StringTypeConverter

@Database(entities =
    [StationCacheEntity::class,
    FareCacheEntity::class,
    FlightCacheEntity::class,
    RegularFareCacheEntity::class,
    TripCacheEntity::class,
    TripDateCacheEntity::class],
    version = 5, exportSchema = false)
@TypeConverters(StringTypeConverter::class)
abstract class ProjectDatabase: RoomDatabase() {

    abstract fun projectDao(): ProjectDao

    companion object {
        val DATABASE_NAME = "project_db"
    }
}