package com.mm.ryanairtechnicaltask.model.network.networkentities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class FareNetworkEntity(

    @SerializedName("amount")
    @Expose
    var amount: Double,

    @SerializedName("publishedFare")
    @Expose
    var publishedFare: Double,

    @SerializedName("discountInPercent")
    @Expose
    var discountInPercent: Double

)