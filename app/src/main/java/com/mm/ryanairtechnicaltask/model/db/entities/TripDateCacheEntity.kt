package com.mm.ryanairtechnicaltask.model.db.entities

import androidx.room.*

@Entity(
    tableName = "tripDate",
    foreignKeys = [
        ForeignKey(
            entity = TripCacheEntity::class,
            parentColumns = ["id"],
            childColumns = ["id"]
        )
    ]
)
data class TripDateCacheEntity(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long,

    @ColumnInfo(name = "dateOut")
    var dateOut: String
) {
    @Ignore
    var flights: List<FlightCacheEntity> = listOf()
}