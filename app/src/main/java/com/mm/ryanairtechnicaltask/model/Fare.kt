package com.mm.ryanairtechnicaltask.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Fare(
    var amount: Double,
    var publishedFare: Double,
    var discountInPercent: Double
): Parcelable