package com.mm.ryanairtechnicaltask.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RegularFare(
    var fareKey: String,
    var fareClass: String,
    var fares: List<Fare>
): Parcelable