package com.mm.ryanairtechnicaltask.model

data class Trip(
    var origin: String,
    var originName: String,
    var destination: String,
    var destinationName: String,
    var dates: List<TripDate>
)