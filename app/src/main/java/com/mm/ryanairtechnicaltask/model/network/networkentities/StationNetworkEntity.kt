package com.mm.ryanairtechnicaltask.model.network.networkentities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class StationNetworkEntity(

    @SerializedName("code")
    @Expose
    var code: String,

    @SerializedName("name")
    @Expose
    var name: String
)