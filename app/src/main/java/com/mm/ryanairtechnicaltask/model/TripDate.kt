package com.mm.ryanairtechnicaltask.model

data class TripDate(
    var dateOut: String,
    var flights: List<Flight>
)