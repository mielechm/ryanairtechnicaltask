package com.mm.ryanairtechnicaltask.model.db.entities

import androidx.room.*
import com.mm.ryanairtechnicaltask.utils.StringTypeConverter

@Entity(
    tableName = "flight",
    foreignKeys = [
        ForeignKey(
            entity = TripDateCacheEntity::class,
            parentColumns = ["id"],
            childColumns = ["flightKey"]
        )
    ]
)
data class FlightCacheEntity(

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "flightKey")
    var flightKey: String,

    @ColumnInfo(name = "infantsLeft")
    var infantsLeft: Int,

    @ColumnInfo(name = "flightNumber")
    var flightNumber: String,

    @ColumnInfo(name = "duration")
    var duration: String,

    @ColumnInfo(name = "time")
    @TypeConverters(StringTypeConverter::class)
    var time: List<String>
)