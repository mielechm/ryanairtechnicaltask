package com.mm.ryanairtechnicaltask.model.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.mm.ryanairtechnicaltask.model.db.entities.StationCacheEntity

@Dao
interface ProjectDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertStation(stationEntity: StationCacheEntity): Long

    @Query("SELECT * FROM stations")
    suspend fun getStations(): List<StationCacheEntity>
}