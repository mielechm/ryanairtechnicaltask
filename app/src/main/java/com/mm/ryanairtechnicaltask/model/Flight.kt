package com.mm.ryanairtechnicaltask.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Flight(
    var flightKey: String,
    var infantsLeft: Int,
    var flightNumber: String,
    var duration: String,
    var time: List<String>,
    var regularFare: RegularFare
): Parcelable