package com.mm.ryanairtechnicaltask.model.network.api

import com.mm.ryanairtechnicaltask.model.network.responses.FlightSearchResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface FlightsService {

    @GET("booking/v4/en-gb/Availability?ToUs=AGREED&Disc=0&inf=0&roundtrip=false&flexdaysout=3&flexdaysin=3&flexdaysbeforeout=3&flexdaysbeforein=3")
    suspend fun getAvailableFlights(
        @Query("origin") origin: String,
        @Query("destination") destination: String,
        @Query("dateout") dateout: String,
        @Query("datein") datein: String,
        @Query("adt") adults: Int,
        @Query("teen") teens: Int,
        @Query("chd") children: Int
        ): FlightSearchResponse

}