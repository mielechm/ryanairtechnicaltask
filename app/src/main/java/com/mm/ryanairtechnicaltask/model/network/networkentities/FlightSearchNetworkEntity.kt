package com.mm.ryanairtechnicaltask.model.network.networkentities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.mm.ryanairtechnicaltask.model.Trip

data class FlightSearchNetworkEntity(

    @SerializedName("currency")
    @Expose
    var currency: String,

    @SerializedName("currPrecision")
    @Expose
    var currPrecision: Int,

    @SerializedName("trips")
    @Expose
    var trips: List<Trip>
)