package com.mm.ryanairtechnicaltask.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FlightOnListRepresentation(
    var flight: Flight,
    var currency: String
): Parcelable