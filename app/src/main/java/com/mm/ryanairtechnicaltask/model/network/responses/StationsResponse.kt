package com.mm.ryanairtechnicaltask.model.network.responses

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.mm.ryanairtechnicaltask.model.network.networkentities.StationNetworkEntity

data class StationsResponse(
    @SerializedName("stations")
    @Expose
    var stations: List<StationNetworkEntity>
)