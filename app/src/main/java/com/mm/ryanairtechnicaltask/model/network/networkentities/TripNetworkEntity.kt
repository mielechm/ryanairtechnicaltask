package com.mm.ryanairtechnicaltask.model.network.networkentities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.mm.ryanairtechnicaltask.model.TripDate

data class TripNetworkEntity(
    @SerializedName("origin")
    @Expose
    var origin: String,

    @SerializedName("originName")
    @Expose
    var originName: String,

    @SerializedName("destination")
    @Expose
    var destination: String,

    @SerializedName("destinationName")
    @Expose
    var destinationName: String,

    @SerializedName("dates")
    @Expose
    var dates: List<TripDate>
)