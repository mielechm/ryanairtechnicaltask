package com.mm.ryanairtechnicaltask.model.network.mappers

import com.mm.ryanairtechnicaltask.model.TripDate
import com.mm.ryanairtechnicaltask.model.network.networkentities.TripDateNetworkEntity
import com.mm.ryanairtechnicaltask.utils.EntityMapper
import javax.inject.Inject

class TripDateNetworkMapper
@Inject
constructor() : EntityMapper<TripDateNetworkEntity, TripDate> {
    override fun mapFromEntity(entity: TripDateNetworkEntity): TripDate {
        return TripDate(
            dateOut = entity.dateOut,
            flights = entity.flights
        )
    }

    override fun mapToEntity(domainModel: TripDate): TripDateNetworkEntity {
        return TripDateNetworkEntity(
            dateOut = domainModel.dateOut,
            flights = domainModel.flights
        )
    }

    fun mapFromEntityList(entities: List<TripDateNetworkEntity>): List<TripDate> {
        return entities.map {
            mapFromEntity(it)
        }
    }
}