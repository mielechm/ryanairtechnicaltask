package com.mm.ryanairtechnicaltask.model.network.mappers

import com.mm.ryanairtechnicaltask.model.Station
import com.mm.ryanairtechnicaltask.model.network.networkentities.StationNetworkEntity
import com.mm.ryanairtechnicaltask.utils.EntityMapper
import javax.inject.Inject

class StationsNetworkMapper
@Inject
constructor() : EntityMapper<StationNetworkEntity, Station> {
    override fun mapFromEntity(entity: StationNetworkEntity): Station {
        return Station(
            code = entity.code,
            name = entity.name
        )
    }

    override fun mapToEntity(domainModel: Station): StationNetworkEntity {
        return StationNetworkEntity(
            code = domainModel.code,
            name = domainModel.name
        )
    }

    fun mapFromEntityList(entities: List<StationNetworkEntity>): List<Station> {
        return entities.map {
            mapFromEntity(it)
        }
    }
}