package com.mm.ryanairtechnicaltask.model.network.networkentities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.mm.ryanairtechnicaltask.model.Fare

data class RegularFareNetworkEntity(

    @SerializedName("fareKey")
    @Expose
    var fareKey: String,

    @SerializedName("faceClass")
    @Expose
    var fareClass: String,

    @SerializedName("fares")
    @Expose
    var fares: List<Fare>
)