package com.mm.ryanairtechnicaltask.model.network.mappers

import com.mm.ryanairtechnicaltask.model.network.networkentities.FlightSearchNetworkEntity
import com.mm.ryanairtechnicaltask.model.network.responses.FlightSearchResponse
import com.mm.ryanairtechnicaltask.utils.EntityMapper
import javax.inject.Inject

class FlightSearchNetworkMapper
@Inject
constructor() : EntityMapper<FlightSearchNetworkEntity, FlightSearchResponse> {
    override fun mapFromEntity(entity: FlightSearchNetworkEntity): FlightSearchResponse {
        return FlightSearchResponse(
            currency = entity.currency,
            currPrecision = entity.currPrecision,
            trips = entity.trips
        )
    }

    override fun mapToEntity(domainModel: FlightSearchResponse): FlightSearchNetworkEntity {
        return FlightSearchNetworkEntity(
            currency = domainModel.currency,
            currPrecision = domainModel.currPrecision,
            trips = domainModel.trips
        )
    }

    fun mapFromEntityList(entities: List<FlightSearchNetworkEntity>): List<FlightSearchResponse> {
        return entities.map {
            mapFromEntity(it)
        }
    }
}