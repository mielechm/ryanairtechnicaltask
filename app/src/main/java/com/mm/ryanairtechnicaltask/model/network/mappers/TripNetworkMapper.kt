package com.mm.ryanairtechnicaltask.model.network.mappers

import com.mm.ryanairtechnicaltask.model.Trip
import com.mm.ryanairtechnicaltask.model.network.networkentities.TripNetworkEntity
import com.mm.ryanairtechnicaltask.utils.EntityMapper
import javax.inject.Inject

class TripNetworkMapper
@Inject
constructor() : EntityMapper<TripNetworkEntity, Trip> {
    override fun mapFromEntity(entity: TripNetworkEntity): Trip {
        return Trip(
            origin = entity.origin,
            destination = entity.destination,
            originName = entity.originName,
            destinationName = entity.destinationName,
            dates = entity.dates
        )
    }

    override fun mapToEntity(domainModel: Trip): TripNetworkEntity {
        return TripNetworkEntity(
            origin = domainModel.origin,
            destination = domainModel.destination,
            originName = domainModel.originName,
            destinationName = domainModel.destinationName,
            dates = domainModel.dates
        )
    }

    fun mapFromEntityList(entities: List<TripNetworkEntity>): List<Trip> {
        return entities.map {
            mapFromEntity(it)
        }
    }
}