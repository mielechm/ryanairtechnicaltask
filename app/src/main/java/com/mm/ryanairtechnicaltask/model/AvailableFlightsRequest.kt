package com.mm.ryanairtechnicaltask.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AvailableFlightsRequest(
    var origin: String,
    var originName: String,
    var destination: String,
    var destinationName: String,
    var date: String,
    var adults: Int,
    var teens: Int,
    var chilrden: Int
) : Parcelable