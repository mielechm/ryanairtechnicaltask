package com.mm.ryanairtechnicaltask.model.network.responses

import com.mm.ryanairtechnicaltask.model.Trip

data class FlightSearchResponse(

    var currency: String,
    var currPrecision: Int,
    var trips: List<Trip>
)