package com.mm.ryanairtechnicaltask.repository

import com.mm.ryanairtechnicaltask.model.Station
import com.mm.ryanairtechnicaltask.model.db.ProjectDao
import com.mm.ryanairtechnicaltask.model.db.mappers.StationCacheMapper
import com.mm.ryanairtechnicaltask.model.network.api.StationsService
import com.mm.ryanairtechnicaltask.model.network.mappers.StationsNetworkMapper
import com.mm.ryanairtechnicaltask.utils.DataState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class StationsRepository
constructor(
    private val projectDao: ProjectDao,
    private val stationsService: StationsService,
    private val stationCacheMapper: StationCacheMapper,
    private val networkMapper: StationsNetworkMapper
){

    fun getStations(): Flow<DataState<List<Station>>> = flow {
        emit(DataState.Loading)

        try {
            val networkStations = stationsService.getStations().stations
            val stations = networkMapper.mapFromEntityList(networkStations)
            for (station in stations) {
                projectDao.insertStation(stationCacheMapper.mapToEntity(station))
            }
            val cachedStations = projectDao.getStations()
            emit(DataState.Success(stationCacheMapper.mapFromEntityList(cachedStations)))

        } catch (e: Exception) {
            emit(DataState.Error(e))
        }
    }
}