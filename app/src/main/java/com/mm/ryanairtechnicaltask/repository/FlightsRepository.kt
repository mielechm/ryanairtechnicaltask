package com.mm.ryanairtechnicaltask.repository

import com.mm.ryanairtechnicaltask.model.AvailableFlightsRequest
import com.mm.ryanairtechnicaltask.model.network.api.FlightsService
import com.mm.ryanairtechnicaltask.model.network.responses.FlightSearchResponse
import com.mm.ryanairtechnicaltask.utils.DataState
import com.mm.ryanairtechnicaltask.utils.toDate
import com.mm.ryanairtechnicaltask.utils.toSimpleString
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.util.*

class FlightsRepository
constructor(
    private val flightsService: FlightsService) {

    fun getAvailableFlights(request: AvailableFlightsRequest): Flow<DataState<FlightSearchResponse>> = flow {
        emit(DataState.Loading)
        try {
            var dateIn = request.date.toDate()
            val calendar = Calendar.getInstance()
            calendar.time = dateIn
            calendar.add(Calendar.DAY_OF_YEAR, 1)
            dateIn = calendar.time
            val response = flightsService.getAvailableFlights(request.origin, request.destination, request.date, dateIn.toSimpleString(), request.adults, request.teens, request.chilrden)
            emit(DataState.Success(response))

        } catch (e: Exception) {
            emit(DataState.Error(e))
        }
    }
}