package com.mm.ryanairtechnicaltask

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class FlightsApplication: Application() {}