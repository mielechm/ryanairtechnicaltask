package com.mm.ryanairtechnicaltask.viewmodel

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.mm.ryanairtechnicaltask.model.Station
import com.mm.ryanairtechnicaltask.repository.StationsRepository
import com.mm.ryanairtechnicaltask.utils.DataState
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class StationsViewModel
@ViewModelInject
constructor(
    private val stationsRepository: StationsRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val _dataState: MutableLiveData<DataState<List<Station>>> = MutableLiveData()

    val dataState: LiveData<DataState<List<Station>>>
        get() = _dataState

    @ExperimentalCoroutinesApi
    fun setStateEvent(stationStateEvent: StationsStateEvent) {
        viewModelScope.launch {
            when (stationStateEvent) {
                is StationsStateEvent.GetStationEvents -> {
                    stationsRepository.getStations().onEach { dataState ->
                        _dataState.value = dataState
                    }
                        .launchIn(viewModelScope)
                }
            }
        }
    }

}

sealed class StationsStateEvent {
    object GetStationEvents : StationsStateEvent()
}