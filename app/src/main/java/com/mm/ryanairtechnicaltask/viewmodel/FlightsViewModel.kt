package com.mm.ryanairtechnicaltask.viewmodel

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.mm.ryanairtechnicaltask.model.AvailableFlightsRequest
import com.mm.ryanairtechnicaltask.model.network.responses.FlightSearchResponse
import com.mm.ryanairtechnicaltask.repository.FlightsRepository
import com.mm.ryanairtechnicaltask.utils.DataState
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class FlightsViewModel
@ViewModelInject
constructor(
    private val flightsRepository: FlightsRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {
    private val _dataState: MutableLiveData<DataState<FlightSearchResponse>> = MutableLiveData()

    val dataState: LiveData<DataState<FlightSearchResponse>>
        get() = _dataState

    @ExperimentalCoroutinesApi
    fun setStateEvent(
        request: AvailableFlightsRequest,
        stateEvent: FlightSearchResponseStateEvent
    ) {
        viewModelScope.launch {
            when (stateEvent) {
                is FlightSearchResponseStateEvent.GetFlightSearchResponseEvents -> {
                    flightsRepository.getAvailableFlights(request).onEach { dataState ->
                        _dataState.value = dataState
                    }
                        .launchIn(viewModelScope)
                }
            }
        }
    }

}

sealed class FlightSearchResponseStateEvent {
    object GetFlightSearchResponseEvents : FlightSearchResponseStateEvent()
}